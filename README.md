This repo contains the companion app compatible with the legacy LEDongle.  
See [here](https://gitlab.com/schadEigentlich/LEDongle2-app) for the new app.

![Imgur](https://i.imgur.com/ltv7qzN.png)