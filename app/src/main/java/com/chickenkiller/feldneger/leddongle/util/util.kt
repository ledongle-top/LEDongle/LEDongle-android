

import android.graphics.Color

fun fullLightnessColor(color: Int): Int{
    val hsv = FloatArray(3)
    Color.colorToHSV(color, hsv)
    hsv[2] = 1.0f
    return Color.HSVToColor(hsv)
}
