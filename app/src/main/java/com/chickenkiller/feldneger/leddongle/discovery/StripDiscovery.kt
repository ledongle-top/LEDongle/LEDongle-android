package com.chickenkiller.feldneger.leddongle.discovery

import android.app.Activity
import android.content.Context
import kotlinx.coroutines.experimental.channels.ReceiveChannel

interface StripDiscovery{
    fun startDiscovery(context: Context): ReceiveChannel<DiscoveryResult<*>>
    fun stopDiscovery()
    fun initialize(activity: Activity): Boolean
}