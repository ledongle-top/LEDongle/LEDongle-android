package com.chickenkiller.feldneger.leddongle.util

data class RGB(val r: Int, val g: Int, val b: Int){
    constructor(color: Int) : this(color.and(0xff0000).shr(16),
            color.and(0xff00).shr(8),
            color.and(0xff))

    fun toInt(): Int
            = r.shl(16).or(g.shl(8)).or(b)

}