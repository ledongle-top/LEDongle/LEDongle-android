package com.chickenkiller.feldneger.leddongle.connection

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import kotlinx.coroutines.experimental.TimeoutCancellationException
import kotlinx.coroutines.experimental.channels.ClosedSendChannelException
import kotlinx.coroutines.experimental.channels.SendChannel
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.withTimeout
import java.util.concurrent.ConcurrentLinkedDeque
import com.chickenkiller.feldneger.leddongle.util.RGB
import android.app.PendingIntent
import android.support.v7.app.NotificationCompat
import com.chickenkiller.feldneger.leddongle.R
import com.chickenkiller.feldneger.leddongle.activities.ControlActivity
import com.chickenkiller.feldneger.leddongle.activities.INTENT_EXTRA_DEVICE_ADDRESS
import com.chickenkiller.feldneger.leddongle.activities.INTENT_EXTRA_SERVICE_INTENT
import java.nio.charset.Charset


private const val TAG = "STRIP"
private val gson = GsonBuilder().create()
abstract class LedStrip: Service(){

    var r: Int = 0
        private set
    var g: Int = 0
        private set
    var b: Int = 0
        private set
    var on: Boolean = false
        private set
    var cmd: String = "none"
        private set
    var length: Int = 0
        private set
    var connected: Boolean = false
        private set
    var address: String? = null

    private val listeners = ConcurrentLinkedDeque<SendChannel<StripListenerMessage>>()

    private val mBuilder: NotificationCompat.Builder = android.support.v7.app.NotificationCompat.Builder(this)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        address = intent?.getStringExtra(INTENT_EXTRA_DEVICE_ADDRESS)
        if(address == null){
            stopSelf()
        }else {
            connect()
        }
        return super.onStartCommand(intent, flags, startId)
    }
    override fun onBind(p0: Intent?): IBinder{

        mBuilder.setContentTitle("LEDongle")
        mBuilder.setSmallIcon(R.drawable.notification_icon)//TODO proper icon
        mBuilder.setContentText("Running in Background")
        val notificationIntent = Intent(this, ControlActivity::class.java)
        notificationIntent.putExtra(INTENT_EXTRA_SERVICE_INTENT, Intent(this, this::class.java))
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        mBuilder.setContentIntent(pendingIntent)
        mBuilder.setOngoing(true)

        startForeground(39, mBuilder.build())
        return StripBinder()
    }

    fun registerListener(channel: SendChannel<StripListenerMessage>){
        listeners.add(channel)
        launch {
            channel.send(StripListenerMessage.ConnectionStateChange(connected))
        }
    }

    fun changeColor(r: Int = -1, g: Int = -1, b: Int = -1){
        val jsonMsg = JsonObject()
        if(r > 255 || b > 255 || g > 255 ||
                r < -1 || g < -1 || b < -1){
            throw(IllegalArgumentException("Invalid color values"))
        }
        if(r != -1) {
            jsonMsg.addProperty("r", r)
        }
        if(g != -1) {
            jsonMsg.addProperty("g", g)
        }
        if(b != -1) {
            jsonMsg.addProperty("b", b)
        }

        sendMessage(jsonMsg.toString(), null)
    }

    fun changeColor(color: Int){
        val rgb = RGB(color)
        changeColor(rgb.r, rgb.g, rgb.b)
    }

    fun changeLength(num: Int){
        if(num < 0){
            throw(IllegalArgumentException("Number must be >= 0"))
        }
        val jsonMsg = JsonObject()
        jsonMsg.addProperty("num", num)
        sendMessage(jsonMsg.toString(), null)
    }

    fun changeEffect(effect: String){
        val msg = JsonObject()
        msg.addProperty("cmd", effect)
        sendMessage(msg.toString(), null)
    }

    private fun updateStripState(state: StripListenerMessage.StripState){
        r = state.r
        g = state.g
        b = state.b
        length = state.num
        cmd = state.cmd
        Log.d(TAG, "new state $state")
    }



    fun requestEffects(){
        val msg = JsonObject()
        msg.addProperty("cmd", "effects")
        sendMessage(msg.toString(), null)
    }

    fun requestStatus(){
        val msg = JsonObject()
        msg.addProperty("cmd", "status")
        sendMessage(msg.toString(), null)
    }

    fun sendRawFrame(frame: ByteArray){
        val msg = JsonObject()
        msg.addProperty("cmd", "RawFrame")
        sendMessage(msg.toString(), frame)
    }

    protected fun sendMessageToListeners(message: StripListenerMessage){
        listeners.forEach {
            launch {
                try {
                    withTimeout(5000) {
                        it.send(message)
                    }
                } catch (e: ClosedSendChannelException) {
                    Log.i(TAG, "removing closed listener channel")
                    listeners.remove(it)
                }catch(_: TimeoutCancellationException){
                    Log.i(TAG, "removing timed out listener channel")
                    listeners.remove(it)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disconnect()
    }

    abstract fun sendMessage(cmd: String, data: ByteArray?)
    abstract fun connect()
    protected fun handleMessage(msg: String){
        if(msg.startsWith("[")){
            val effects = gson.fromJson<List<String>>(msg)
            sendMessageToListeners(StripListenerMessage.EffectsList(effects))
        }else{
            val status:StripListenerMessage.StripState? = gson.fromJson(msg)
            if(status != null) {
                updateStripState(status)
                sendMessageToListeners(status)
            }
        }
    }
    inner class StripBinder: Binder(){
        val service = this@LedStrip
    }
    abstract fun disconnect()
}

sealed class StripListenerMessage {
    data class StripState(val cmd: String, val r: Int, val g: Int, val b: Int, val num: Int): StripListenerMessage()
    data class EffectsList(val effects: List<String>): StripListenerMessage()
    data class ConnectionStateChange(val state: Boolean): StripListenerMessage()
    data class StripError(val message: String): StripListenerMessage()
}
