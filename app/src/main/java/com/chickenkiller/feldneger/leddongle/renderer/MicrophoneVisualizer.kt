package com.chickenkiller.feldneger.leddongle.renderer

import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import android.util.Log
import com.chickenkiller.feldneger.leddongle.connection.LedStrip
import com.paramsen.noise.Noise
import kotlinx.coroutines.experimental.*
import java.util.*
import java.util.concurrent.LinkedBlockingDeque
import kotlin.math.ceil
import kotlin.math.max
import kotlin.math.min

private const val SAMPLING_RATE = 44100
private const val FRAME_RATE = 15
private const val TAG = "MUSIC"
private const val amplitudeWindowSize = 60
private const val ceilingRaiseThreshold = 1.0
private const val ceilingRaiseFactor = 2.0
private const val ceilingLowerThreshold = 0.33
private const val ceilingLowerFactor = 0.66
private const val smoothingFactor = 0.8

class MicrophoneVisualizer(override val strip: LedStrip): Renderer {

    private val visualizerCoroutine: Job?
    private val audioRecord: AudioRecord
    private val frame: ByteArray
    private var average = .0f
    private var amplitudeCeiling = 1750000000
    private val movingAverages = LinkedBlockingDeque<Float>()


    init {
        val samplesPerFrame = SAMPLING_RATE / FRAME_RATE

        val bytesPerFrame = samplesPerFrame * 2

        val audioBufferSize = max(bytesPerFrame * 10,
                    android.media.AudioRecord.getMinBufferSize(SAMPLING_RATE,
                            AudioFormat.CHANNEL_IN_MONO,
                            AudioFormat.ENCODING_PCM_16BIT))


        audioRecord = AudioRecord(
                MediaRecorder.AudioSource.CAMCORDER,
                SAMPLING_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                audioBufferSize)
        val sampleBuffer = ShortArray(samplesPerFrame)
        frame = ByteArray(strip.length*3, {i -> when(i%3){
            0 -> strip.g.toByte()
            1-> strip.r.toByte()
            else -> strip.b.toByte()
        }})
        if(audioRecord.state == AudioRecord.STATE_INITIALIZED){
            audioRecord.startRecording()
            val noiseFft = Noise.real().optimized().init(samplesPerFrame, true)
            visualizerCoroutine = launch{
                while (isActive) {
                    audioRecord.read(sampleBuffer, 0, samplesPerFrame, AudioRecord.READ_BLOCKING)
                    val fftOutput = noiseFft.fft(sampleBuffer.map { it.toFloat() }.toFloatArray())
                    val fft = fftOutput.slice(IntRange(2,fftOutput.size-2))
                    val bands = strip.length
                    val bandSize = samplesPerFrame / bands
                    var bassAvg = 0.0f
                    for (i in 0 until bands) {
                        var accum = .0f

                        synchronized(fft) {
                            for (j in 0 until bandSize step 2) {
                                //convert real and imag part to get energy
                                accum += ((Math.pow(fft[j + (i * bandSize)].toDouble(), 2.0) + Math.pow(fft[j + 1 + (i * bandSize)].toDouble(), 2.0)) * Math.log((i+1).toDouble())).toFloat()
                            }

                            accum /= bandSize / 2
                        }

                        average += accum
                        //val scale = min(accum/ amplitudeCeiling.toDouble(), 1.0)
                        if(i == 1){
                            bassAvg += accum
                        }

                    }


                    average /= bands
                    movingAverages.addLast(average)
                    val scale = bassAvg / amplitudeCeiling
                    for(i in 0 until (strip.length-1)){
                        frame[i*3] = ((strip.g * scale) * (1.0 - smoothingFactor) + frame[i*3].toDouble() * smoothingFactor ).toByte()
                        frame[i*3+1] = ((strip.r * scale)* (1.0 - smoothingFactor) + frame[i*3+1].toDouble() * smoothingFactor ).toByte()
                        frame[i*3+2] = ((strip.b * scale)* (1.0 - smoothingFactor) + frame[i*3+2].toDouble() * smoothingFactor ).toByte()
                    }


                    if(movingAverages.size >= amplitudeWindowSize) {
                        val windowAverage = movingAverages.average()
                        movingAverages.drop(movingAverages.size - amplitudeWindowSize)
                        if (windowAverage  > amplitudeCeiling * ceilingRaiseThreshold ) {
                            amplitudeCeiling = (windowAverage * ceilingRaiseFactor).toInt()
                            Log.d(TAG, "adjust ceiling up to $amplitudeCeiling")
                        }else if(windowAverage < amplitudeCeiling * ceilingLowerThreshold){
                            amplitudeCeiling = (windowAverage * ceilingLowerFactor).toInt()
                            Log.d(TAG, "adjust ceiling down to $amplitudeCeiling")
                        }
                    }

                    Log.d(TAG, "avg. amplitude $average")
                    strip.sendRawFrame(frame)
                }
            }

        }else{
            visualizerCoroutine = null
        }


    }
    override fun stop() {
        runBlocking {
            visualizerCoroutine?.cancelAndJoin()
        }

        audioRecord.stop()
    }

}