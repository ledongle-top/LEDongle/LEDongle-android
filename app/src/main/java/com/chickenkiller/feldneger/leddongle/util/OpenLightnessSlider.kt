package com.chickenkiller.feldneger.leddongle.util

import android.content.Context
import android.util.AttributeSet
import com.flask.colorpicker.slider.LightnessSlider


class OpenLightnessSlider: LightnessSlider{
    constructor(c: Context): super(c)
    constructor(c: Context, attrs: AttributeSet): super(c, attrs)
    constructor(c: Context, attrs: AttributeSet, defStyleAttrs: Int): super(c, attrs, defStyleAttrs)
    val lightness: Float
    get() = value
}