package com.chickenkiller.feldneger.leddongle.discovery

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.bluetooth.le.ScanSettings.*
import android.content.Context
import android.content.Context.BLUETOOTH_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Toast
import com.chickenkiller.feldneger.leddongle.connection.BLEStrip
import com.chickenkiller.feldneger.leddongle.connection.DONGLE_SERVICE_UUID
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.ClosedSendChannelException
import kotlinx.coroutines.experimental.channels.ReceiveChannel
import kotlinx.coroutines.experimental.channels.SendChannel
import kotlinx.coroutines.experimental.runBlocking
import java.util.*

private const val COARSE_LOCATION_REQUEST = 1
private const val ENABLE_BLUETOOTH_REQUEST = 2

private const val TAG = "BLEDiscover"
object BLEDiscovery: StripDiscovery{
    private lateinit var mBluetoothAdapter: BluetoothAdapter
    private val runningScanCallbacks = Collections.synchronizedList(ArrayList<BTScanCallback>())

    override fun initialize(activity: Activity): Boolean {
        mBluetoothAdapter = (activity.getSystemService(BLUETOOTH_SERVICE) as BluetoothManager).adapter
        Log.d(TAG, "Initializing BLE")
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Location permission missing")
                Toast.makeText(activity, "Location is needed for BLE scan!", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), COARSE_LOCATION_REQUEST)
            }
            if (!activity.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
                Log.d(TAG, "BLE is not supported")
                Toast.makeText(activity, "BLE not supported", Toast.LENGTH_SHORT).show()
                return false
            }
            val mBluetoothAdapter: BluetoothAdapter? = (activity.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter
            if (mBluetoothAdapter?.isEnabled == false) {
                Log.d(TAG, "Bluetooth is disabled")
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                activity.startActivityForResult(enableBtIntent, ENABLE_BLUETOOTH_REQUEST)//TODO check if bt was enabled
            }
            return mBluetoothAdapter?.isEnabled == true
    }

    override fun stopDiscovery() {
        runningScanCallbacks.forEach{ mBluetoothAdapter.bluetoothLeScanner.stopScan(it)}
        runningScanCallbacks.clear()
    }



    override fun startDiscovery(context: Context): ReceiveChannel<DiscoveryResult<BLEStrip>> {
        Log.d(TAG, "starting scan")
       // Exception().printStackTrace()
        val channel = Channel<DiscoveryResult<BLEStrip>>()
        val scanSettings = ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setCallbackType( CALLBACK_TYPE_ALL_MATCHES )
                .build()
        val filter = ScanFilter.Builder().setServiceUuid(DONGLE_SERVICE_UUID).build()
        val filterList = listOf(filter)
        stopDiscovery()
        val scanCallback =  BTScanCallback(channel, context)
        mBluetoothAdapter.bluetoothLeScanner.startScan(filterList, scanSettings, scanCallback)
        runningScanCallbacks.add(scanCallback)
        return channel
    }


    class BTScanCallback(private val channel: SendChannel<DiscoveryResult<BLEStrip>>, val context: Context): ScanCallback(){
        private val discoveredDevices = LinkedList<String>()
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)
            if(discoveredDevices.contains(result.device.address)) return //Ignore known devices
            discoveredDevices.add(result.device.address)
            val discoveryResult = DiscoveryResult<BLEStrip>(result.device.name, result.device.address, context, BLEStrip::class.java)
            Log.d(TAG, result.toString())
            runBlocking {
                try{
                    channel.send(discoveryResult)
                }catch(_: ClosedSendChannelException){
                    Log.i(TAG, "Tried to write to closed channel")
                }
            }
        }

    }

}

