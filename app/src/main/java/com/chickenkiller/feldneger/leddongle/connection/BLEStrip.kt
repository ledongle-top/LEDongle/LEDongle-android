package com.chickenkiller.feldneger.leddongle.connection

import android.bluetooth.*
import android.content.Context
import android.os.ParcelUuid
import android.util.Log
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.sync.Mutex
import java.io.ByteArrayOutputStream

val DONGLE_CHARACTERISTIC_UUID: ParcelUuid = ParcelUuid.fromString("0000FFE1-0000-1000-8000-00805F9B34FB")
val DONGLE_SERVICE_UUID: ParcelUuid = ParcelUuid.fromString("0000FFE0-0000-1000-8000-00805F9B34FB")
val DONGLE_DESCRIPTOR_UUID: ParcelUuid = ParcelUuid.fromString("00002902-0000-1000-8000-00805f9b34fb")


private const val TAG = "BTStrip"
private const val BLE_MTU = 23 - 3
class BLEStrip: LedStrip(){

    private val bluetoothAdapter by lazy{(getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter}
    private lateinit var device: BluetoothDevice
    private lateinit var bluetoothGatt: BluetoothGatt
    private lateinit var dongleService: BluetoothGattService
    private lateinit var dongleTxCharacteristic: BluetoothGattCharacteristic
    private var dongleConnected = false

    private val txBuffer = Channel<Byte>(200)
    private val BLEMutex = Mutex()

    override fun sendMessage(cmd: String, data: ByteArray?){
        runBlocking {
            withTimeout(1000){
                cmd.forEach{ txBuffer.send(it.toByte())}
                txBuffer.send('\n'.toByte())
                data?.forEach{txBuffer.send(it)}
            }


        }
    }

    @Suppress("UsePropertyAccessSyntax")
    fun txCoroutine() = launch{
        val buffer = ByteArrayOutputStream(BLE_MTU)
        while(isActive) {
            BLEMutex.lock()

            buffer.write(txBuffer.receive().toInt()) //wait here if there is no data
            for (bufferIndex in 1 until BLE_MTU) {
                if (!txBuffer.isEmpty) {
                    buffer.write(txBuffer.receive().toInt())
                } else {
                    break
                }
            }


            Log.d(TAG, "sending $buffer")
            dongleTxCharacteristic.setValue(buffer.toByteArray())
            bluetoothGatt.writeCharacteristic(dongleTxCharacteristic)
            buffer.reset()
        }
    }

    override fun connect() {
        if(!connected) {
            Log.d(TAG, "connecting")
            device = bluetoothAdapter.getRemoteDevice(address)
            bluetoothGatt = device.connectGatt(this, false, bluetoothGattCallback)
        }else{
            sendMessageToListeners(StripListenerMessage.ConnectionStateChange(true))
            Log.d(TAG, "already connected")
        }
    }

    override fun disconnect() {
        Log.d(TAG, "disconnect")
        bluetoothGatt.disconnect()
        bluetoothGatt.close()
    }



    private val bluetoothGattCallback: BluetoothGattCallback = object : BluetoothGattCallback() {
        private var rxBuffer = StringBuilder(200)
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            if (newState == BluetoothProfile.STATE_CONNECTED){
                Log.d(TAG, "GATT connected")
                gatt.discoverServices()
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.d(TAG, "GATT disconnected")
                sendMessageToListeners(StripListenerMessage.ConnectionStateChange(false))
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            if(gatt == null) {
                Log.e(TAG, "GATT in callback is NULL!")
                return
            }
            dongleService = gatt.getService(DONGLE_SERVICE_UUID.uuid)
            dongleTxCharacteristic = dongleService.getCharacteristic(DONGLE_CHARACTERISTIC_UUID.uuid)
            dongleConnected = true
            Log.d(TAG, "Services discovered")
            if(bluetoothGatt.setCharacteristicNotification(dongleTxCharacteristic, true)) {
                txCoroutine()
                runBlocking {
                    delay(500)//FIXME stupid hack
                    sendMessageToListeners(StripListenerMessage.ConnectionStateChange(true))
                }
            }else{
                Log.e(TAG, "Enabling Characteristic Notification failed!")
                sendMessageToListeners(StripListenerMessage.StripError("Failed to receive data from strip"))
            }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            super.onCharacteristicChanged(gatt, characteristic)
            val newData = characteristic?.getStringValue(0)?: return
            Log.d(TAG,"new data $newData")
            newData.forEach {
                when(it.toInt()){
                    0x2 -> rxBuffer.setLength(0)
                    0x3 -> handleMessage(rxBuffer.toString())
                    else -> rxBuffer.append(it)
                }
            }
        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicWrite(gatt, characteristic, status)
            BLEMutex.unlock()
        }
    }



}