package com.chickenkiller.feldneger.leddongle.activities


import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Context
import android.content.Intent
import android.widget.Toast
import android.widget.TextView
import android.widget.ArrayAdapter
import android.widget.AdapterView
import android.os.*
import android.util.Log
import android.view.*
import com.chickenkiller.feldneger.leddongle.discovery.BLEDiscovery
import com.chickenkiller.feldneger.leddongle.R
import com.chickenkiller.feldneger.leddongle.discovery.DiscoveryResult
import com.chickenkiller.feldneger.leddongle.discovery.StripDiscovery
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import java.util.*

private const val TAG: String = "SCAN"

const val INTENT_EXTRA_DEVICE_ADDRESS = "deviceAddress"
const val INTENT_EXTRA_SERVICE_INTENT = "serviceBinder"

class MainActivity : AppCompatActivity() {
    private val scanners = listOf(BLEDiscovery)
    private val initializedScanners = LinkedList<StripDiscovery>()
    private val scanResultList = Collections.synchronizedList(LinkedList<DiscoveryResult<*>>())
    private val adapter: ScanResultAdapter by lazy{ScanResultAdapter(this, android.R.layout.simple_list_item_1, scanResultList)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Initialize scan result list
        bt_scan_list.adapter = adapter
        bt_scan_list.onItemClickListener = AdapterView.OnItemClickListener{adapter, _, index, _ ->
            val result = adapter.getItemAtPosition(index) as DiscoveryResult<*>
            val serviceIntent = result.connect()
            val controlActivityIntent = Intent(this@MainActivity, ControlActivity::class.java)
            controlActivityIntent.putExtra(INTENT_EXTRA_SERVICE_INTENT, serviceIntent)
            startActivity(controlActivityIntent)
        }
    }

    override fun onStart() {
        super.onStart()
        initializeDiscovery()
        Log.d(TAG,"start")
    }

    /**
     * Try initializing initializedScanners and start searching for LEDongles
     * Close the app if none could be initialized
     */
    private fun startDiscovery(){
        stopDiscovery()
        scanResultList.clear()
        adapter.notifyDataSetChanged()
        if(initializedScanners.isEmpty()){
            Toast.makeText(this, "No discovery modules could be initialized", Toast.LENGTH_LONG).show()
            finish()
        }
        initializedScanners.forEach { receiveDiscoveryResults(it) }
    }

    private fun stopDiscovery(){
        initializedScanners.forEach { it.stopDiscovery() }
    }

    private fun initializeDiscovery(){
        initializedScanners.addAll(scanners.filter{ it !in initializedScanners }.filter{ it.initialize(this) })
    }

    private fun receiveDiscoveryResults(scanner: StripDiscovery) = launch{
        val channel = scanner.startDiscovery(this@MainActivity)
        for( res in channel){
            launch(UI) {
                scanResultList.add(res)
                adapter.notifyDataSetChanged()
            }
        }
    }


    override fun onResume() {

        super.onResume()
        startDiscovery()
    }


    override fun onPause() {
        super.onPause()
        initializedScanners.forEach { it.stopDiscovery() }
    }



    internal inner class ScanResultAdapter(context: Context, resource: Int, private var scanResults: List<DiscoveryResult<*>>) : ArrayAdapter<DiscoveryResult<*>>(context, resource, scanResults) {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {//TODO check if convertView can be of type View!
            val view = convertView ?: (getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(android.R.layout.simple_list_item_1, null)
            val textView = view!!.findViewById(android.R.id.text1) as TextView
            textView.text = "${scanResults[position].name}\n${scanResults[position].address}"
            return view
        }
        override fun getItem(pos: Int): DiscoveryResult<*> = scanResults[pos]
    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.scanmenu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
            when(item?.itemId){
                R.id.action_rescan -> {
                    startDiscovery()
                    true
                }
                else -> false
            }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

    }
 }
