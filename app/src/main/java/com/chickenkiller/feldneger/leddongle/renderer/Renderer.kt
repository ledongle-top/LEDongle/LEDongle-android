package com.chickenkiller.feldneger.leddongle.renderer

import com.chickenkiller.feldneger.leddongle.connection.LedStrip

interface Renderer {
    val strip: LedStrip
    fun stop()
}