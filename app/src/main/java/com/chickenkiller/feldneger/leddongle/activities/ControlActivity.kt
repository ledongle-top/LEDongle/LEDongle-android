package com.chickenkiller.feldneger.leddongle.activities

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.widget.Toast

import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_control.*

import android.text.Editable
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.chickenkiller.feldneger.leddongle.R
import com.chickenkiller.feldneger.leddongle.connection.BLEStrip
import com.chickenkiller.feldneger.leddongle.connection.LedStrip
import com.chickenkiller.feldneger.leddongle.connection.StripListenerMessage
import com.chickenkiller.feldneger.leddongle.renderer.MicrophoneVisualizer
import com.chickenkiller.feldneger.leddongle.renderer.Renderer
import com.chickenkiller.feldneger.leddongle.util.RGB
import fullLightnessColor
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.launch


private const val TAG = "CONTROL"
const val BUNDLE_COLOR = "selectedColor"
const val BUNDLE_EFFECT = "selectedEffect"

private const val RECORD_AUDIO_REQUEST = 39
private const val MODIFY_AUDIO_SETTINGS_REQUEST = 42

class ControlActivity : AppCompatActivity() {
    private lateinit var strip: LedStrip
    private var renderer: Renderer? = null

    private val spinnerAdapter by lazy{ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item)}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_control)
        Toast.makeText(this, "Connecting to device. Please Wait...", Toast.LENGTH_LONG).show()

        val savedColor = savedInstanceState?.getInt(BUNDLE_COLOR)
        if(savedColor != null){
            color_picker_view.setInitialColor(savedColor, false)
        }
        val savedCommand = savedInstanceState?.getInt(BUNDLE_EFFECT)
        if(savedCommand != null){
            spinner.setSelection(savedCommand)
        }

        color_picker_view.addOnColorChangedListener(::colorPickerAction)


        spinner.adapter = spinnerAdapter



        ledNumberInput.setOnEditorActionListener { textView, _, _ ->
                    strip.changeLength(textView.text.toString().toIntOrNull()?:0)
            true
        }

        v_lightness_slider.setOnValueChangedListener{ value ->
            val adjustedColor =  com.flask.colorpicker.Utils.colorAtLightness(color_picker_view.selectedColor, value)
            strip.changeColor(adjustedColor)
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "start")
        val serviceIntent = intent.getParcelableExtra<Intent>(INTENT_EXTRA_SERVICE_INTENT)
        bindService(serviceIntent, stripServiceConnection, Context.BIND_AUTO_CREATE)
    }
    private fun colorPickerAction(color: Int){
        val adjustedColor =  com.flask.colorpicker.Utils.colorAtLightness(color, v_lightness_slider.lightness)
        Log.d(TAG, "adjusted $color to $adjustedColor")
        v_lightness_slider.setColor(adjustedColor)
        strip.changeColor(adjustedColor)
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(BUNDLE_COLOR, color_picker_view.selectedColor)
        outState?.putInt(BUNDLE_EFFECT, spinner.selectedItemPosition)
    }

    private fun startMicRenderer(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Audio permission missing")
            Toast.makeText(this, "Please grant Mic access for visualizer", Toast.LENGTH_SHORT).show()
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.RECORD_AUDIO), RECORD_AUDIO_REQUEST)
        }else{
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.MODIFY_AUDIO_SETTINGS) != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Audio permission missing")
                Toast.makeText(this, "Please grant Mic access for visualizer", Toast.LENGTH_SHORT).show()
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.MODIFY_AUDIO_SETTINGS), MODIFY_AUDIO_SETTINGS_REQUEST)
            }
            //val vis = Visualizer(0)
            renderer = MicrophoneVisualizer(strip)
        }
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
            when(item?.itemId){
                R.id.action_power -> {
                    strip.sendMessage("off", null)
                    true
                }
                R.id.action_music -> {
                    if(renderer is MicrophoneVisualizer){
                        renderer?.stop()
                        renderer = null
                    }else{
                        renderer?.stop()
                        startMicRenderer()
                    }


                    true
                }
                else -> false
            }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.actionmenu, menu)
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(stripServiceConnection)
        if(isFinishing){
            Log.d(TAG, "finishing")
            val intent = Intent(this, BLEStrip::class.java)
            stopService(intent)
        }else{
            Log.d(TAG, "not finishing")
        }
    }

    private val stripServiceConnection = object: ServiceConnection{

        override fun onServiceDisconnected(p0: ComponentName?) {
            Log.d(TAG, "strip service disconnected")
        }

        override fun onServiceConnected(p0: ComponentName?, binder: IBinder) {
            Log.d(TAG, "Strip service connected")
            if(binder is LedStrip.StripBinder) {
                this@ControlActivity.strip = binder.service
                val channel = Channel<StripListenerMessage>()
                strip.registerListener(channel)
                launch{
                    for(msg in channel){
                        when(msg){
                            is StripListenerMessage.ConnectionStateChange -> {
                                launch(UI){
                                    statusText.text = if(msg.state) "Connected" else "Disconnected"
                                }
                                if(msg.state){
                                    strip.requestEffects()
                                }
                            }
                            is StripListenerMessage.StripState -> {
                                launch(UI) {
                                    if(spinner.onItemSelectedListener == null) {
                                        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                            override fun onItemSelected(spinner: AdapterView<*>?, item: View?, p2: Int, p3: Long) {
                                                Log.d(TAG, "Spinner onItemSelected: ${(item as TextView).text}")
                                                strip.changeEffect(item.text.toString())
                                            }

                                            override fun onNothingSelected(p0: AdapterView<*>?) {
                                                Log.d("SPINNER", "nothing")
                                            }
                                        }
                                    }
                                    spinner.setSelection(spinnerAdapter.getPosition(msg.effect))
                                    color_picker_view.setColor(fullLightnessColor(RGB(msg.r, msg.g, msg.b).toInt()), false)
                                    v_lightness_slider.setColor(RGB(msg.r, msg.g, msg.b).toInt())
                                    ledNumberInput.text = Editable.Factory().newEditable(msg.num.toString())

                                }
                            }
                            is StripListenerMessage.EffectsList -> {
                                Log.d(TAG, "effects! ${msg.effects}")
                                launch(UI){
                                    val spinnerListener = spinner.onItemSelectedListener
                                    spinner.onItemSelectedListener = null
                                    spinnerAdapter.addAll(msg.effects)
                                    spinnerAdapter.notifyDataSetChanged()
                                    spinner.onItemSelectedListener = spinnerListener
                                }
                                strip.requestStatus()
                            }
                            is StripListenerMessage.StripError -> {
                                Toast.makeText(this@ControlActivity, msg.message, Toast.LENGTH_LONG).show()
                                this@ControlActivity.finish()
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            RECORD_AUDIO_REQUEST -> {
                startMicRenderer()
            }
            MODIFY_AUDIO_SETTINGS_REQUEST -> {
                startMicRenderer()
            }
        }
    }
}
