package com.chickenkiller.feldneger.leddongle.discovery

import android.content.Context
import android.content.Intent
import android.os.IBinder
import com.chickenkiller.feldneger.leddongle.activities.INTENT_EXTRA_DEVICE_ADDRESS
import com.chickenkiller.feldneger.leddongle.connection.LedStrip

class DiscoveryResult<T: LedStrip>(val name: String, val address: String, val context: Context, val type: Class<T>){
    //start the service to connect to a discovered strip. return the service's intent for others to bind to it
    fun connect(): Intent{
        val intent = Intent(context, type)
        intent.putExtra(INTENT_EXTRA_DEVICE_ADDRESS, address)
        if(android.os.Build.VERSION.SDK_INT >= 26) {
            context.startForegroundService(intent)
        }else{
            context.startService(intent)
        }
        return intent
    }
}